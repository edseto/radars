package cat.dam.edgar.radars;

public class Radar
{
    private String tipus;
    private double latitude, longitude;
    private int velocity;

    // Getters
    public String getTipus() {
        return tipus;
    }
    public double getLatitude() {
        return latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public int getVelocity() {
        return velocity;
    }

    // Setters
    public void setTipus(String tipus) {
        this.tipus = tipus;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    // Constructors
    public Radar() { }

    public Radar(double latitude, double longitude, int velocity)
    {
        this.latitude = latitude;
        this.longitude = longitude;
        this.velocity = velocity;
    }
}
