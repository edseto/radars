package cat.dam.edgar.radars;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = new GoogleFragment();
        //Obre fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_mapa, fragment)
                .commit();
    }

}