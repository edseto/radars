package cat.dam.edgar.radars;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GoogleFragment extends Fragment {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 77; //numero indiferent i únic
    SupportMapFragment supportMapFragment;
    private long UPDATE_INTERVAL = 10000;  /* 10 segons */
    private long FASTEST_INTERVAL = 5000; /* 5 segons */
    private double DEFAULT_LAT = 42.1152668, DEFAULT_LONG = 2.7656192; //Ubicació per defecte (Banyoles)
    private int MAP_ZOOM = 10; //ampliació de zoom al marcador (més gran, més zoom)
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationCallback locationCallback;
    private ArrayList<MarkerOptions> marcadors;
    private final ArrayList<Radar> radars = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inicialitza view
        View view = inflater.inflate(R.layout.fragment_google, container, false);

        //Initialitza fragment mapa
        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);

        //Mapa asíncron
        supportMapFragment.getMapAsync((new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                // Podem afegir marcadors
                addMarcadors();

                for (MarkerOptions marker : marcadors) {
                    googleMap.addMarker(marker);
                }
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                LatLng latLngDefault = new LatLng(DEFAULT_LAT,DEFAULT_LONG);
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLngDefault)); //es situa a la posició per defecte
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(MAP_ZOOM)); //ampliació extra d'aproximació
            }
        }));
        if (tePermisUbicacio()) {
            //Inicialitza localització
            obteUbicacio();
        }

        //Retorna view
        return view;
    }

    private void addMarcadors()
    {
        parseCSV();
        marcadors = new ArrayList<>();
        double latitud, longitud;
        String title;

        for (Radar radar: radars) {
            latitud = radar.getLatitude();
            longitud = radar.getLongitude();
            title = radar.getTipus();
            LatLng position = new LatLng(latitud, longitud);
            marcadors.add(new MarkerOptions().position(position).title(title).icon(BitmapDescriptorFactory.fromResource(getIcon(radar.getVelocity()))));
        }
    }

    private int getIcon(int velocity)
    {
        Resources res = getResources();
        return velocity == 100 ? res.getIdentifier("speed_100", "drawable", "cat.dam.edgar.radars") : res.getIdentifier("speed_90", "drawable", "cat.dam.edgar.radars");
    }

    private void parseCSV()
    {
        try {
            InputStream is = getResources().openRawResource(R.raw.radars);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            String[] row;
            reader.readLine(); // Skip first line
            while ((line = reader.readLine())!= null){
                Radar radar = new Radar();
                row = line.split(",");
                radar.setTipus(row[0]);
                radar.setVelocity(Integer.parseInt(row[1]));
                radar.setLongitude(Double.parseDouble(row[2]));
                radar.setLatitude(Double.parseDouble(row[3]));

                radars.add(radar);
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean tePermisUbicacio() {
        if (ContextCompat.checkSelfPermission(this.getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this.getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this.getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private void obteUbicacio() {
        //Comprova permisos i inicialitza tasca d'ubicació
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            tePermisUbicacio();
            return;
        }
        //Inicialitza l'objecte necessari per conèixer la ubicació
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        //Configura l'actualització de les peticions d'ubicació'
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(UPDATE_INTERVAL);
        //Aquest mètode estableix la velocitat en mil·lisegons en què l'aplicació prefereix rebre actualitzacions d'ubicació. Tingueu en compte que les actualitzacions d'ubicació poden ser una mica més ràpides o més lentes que aquesta velocitat per optimitzar l'ús de la bateria, o pot ser que no hi hagi actualitzacions (si el dispositiu no té connectivitat, per exemple).
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        //Aquest mètode estableix la taxa més ràpida en mil·lisegons en què la vostra aplicació pot gestionar les actualitzacions d'ubicació. A menys que la vostra aplicació es beneficiï de rebre actualitzacions més ràpidament que la taxa especificada a setInterval (), no cal que toqueu a aquest mètode.
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                    }
                }
            }
        };
        // Si volem actualitzacions periodiques
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                //Quan tingui permisos crida al mètode
                obteUbicacio();
            }
        }

    }
}